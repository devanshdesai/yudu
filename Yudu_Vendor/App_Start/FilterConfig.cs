﻿using System.Web;
using System.Web.Mvc;

namespace Yudu_Vendor
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
